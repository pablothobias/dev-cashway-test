import { AuthService } from './../services/auth-service/auth.service';
import { Component, OnInit } from '@angular/core';

import { BalanceService } from './../services/balance/balance.service';
import { Observable } from 'rxjs';

export interface Balance {
  available_limit: number;
  balance: number;
  blocked_amount: number;
  limit: number;
}

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page implements OnInit{

  balance: Balance;
  username: string;

  constructor(private balanceService: BalanceService) {}

  ngOnInit() {}

  ionViewWillEnter(){
    this.getUsersName();
    this.fetchUserBalance();
  }

  getUsersName() {
    this.username = (AuthService.getAuthenticatedUser()).data.name;
  }

  fetchUserBalance () {
    this.balanceService.fetchUserBalance()
      .subscribe(res => this.balance = res);
  }
}
