import { AuthService } from './../services/auth-service/auth.service';
import { Injectable, NgZone } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthHeaderInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private zone: NgZone,
    private toastController: ToastController
  ) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if (AuthService.userIsLogged()) {

      const authToken = AuthService.getAuthToken();
      const client = AuthService.getClient();
      const uid = AuthService.getAuthenticatedUser().data.uid;

      console.log(authToken,
        client,
        uid);

      request = request.clone({
        setHeaders: {
          'Access-Control-Allow-Origin': '*',
          'client': client,
          'uid': uid,
          'access-token': authToken
        }
      });
    }

    return next.handle(request)
      .pipe(
        catchError(err => {
          if (err.status == 401 && err.url.includes('sign_in')) {
            this.presentToast();
          }
          if (err.status == 401 && !err.url.includes('sign_in')) {
            const { holder, account } = (AuthService.getAuthenticatedUser()).data;
            const credentials = { holder, account, password: '11111111' };
            localStorage.clear();
            this.authService.login(credentials).toPromise();
            this.zone.run(() => {
              console.log('force update the screen');
            });
            return throwError(err);
          }
        })
      );
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Incorrect credentials. Try again',
      duration: 2000
    });
    toast.present();
  }
}
