import { AuthService } from './../services/auth-service/auth.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

import { StatementsService } from '../services/statements/statements.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page implements OnInit{

  statements$: Observable<any[]>;
  username: string;

  constructor(
    private statementService: StatementsService
  ) {}

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.statements$ = this.statementService.fetchUserStatements();
    this.getUsersName();
  }

  getUsersName() {
    this.username = (AuthService.getAuthenticatedUser()).data.name;
  }

  
}
