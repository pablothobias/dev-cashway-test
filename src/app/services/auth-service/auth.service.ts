import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  uri = `${environment.URI_API}/auth/sign_in`;

  constructor(
    private http: HttpClient,
    private router: Router,
  ) { }

  static userIsLogged() {
   return !!(localStorage.getItem('access-token'));
  }

  static removeTokens() {
    localStorage.clear();
  }

  static getAuthToken() {
    return localStorage.getItem('access-token') ? localStorage.getItem('access-token'): null;
  }

  static getClient() {
    return localStorage.getItem('client') ? localStorage.getItem('client') : null;
  }

  static getAuthenticatedUser() {
    return JSON.parse(localStorage.getItem('authUserData')) ? JSON.parse(localStorage.getItem('authUserData')): null;
  }

  login(credentials): Observable<HttpResponse<Object>> {

    return this.http.post<HttpResponse<Object>>(this.uri, credentials, {observe: 'response'})
      .pipe(
        tap((res: HttpResponse<Object>) => {
          localStorage.setItem('access-token', res.headers.get('access-token'));
          localStorage.setItem('client', res.headers.get('client'));
          localStorage.setItem('authUserData', JSON.stringify(res.body));
        })
      );
  }

  logout() {
    AuthService.removeTokens();
    this.router.navigate(['/login']);
  }
}
