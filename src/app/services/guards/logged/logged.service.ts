import { Injectable } from "@angular/core";
import { Router, CanActivate, ActivatedRouteSnapshot } from "@angular/router";

import { AuthService } from './../../auth-service/auth.service';

@Injectable({
  providedIn: "root"
})
export class LoggedGuardService implements CanActivate {
  constructor(
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot): boolean {

    let authInfo = {
      authenticated: AuthService.userIsLogged()
    };

    if (authInfo.authenticated) {
      this.router.navigate(["home/tabs/tab1"]);
      return false;
    }

    return true;
  }
}