import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BalanceService {

  uri = `${environment.URI_API}/user/balance`;

  constructor(private http: HttpClient) { }

  fetchUserBalance(): Observable<any> {
    return this.http.get(this.uri);
  }
}
