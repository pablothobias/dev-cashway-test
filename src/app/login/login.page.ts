import { Router } from '@angular/router';
import { AuthService } from './../services/auth-service/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private toastController: ToastController
  ) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.loginForm = this.formBuilder.group({
      holder: [null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(3),
      ]
      ],
      account: [null, [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(2),
      ]
      ],
      password: [null, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(8),
      ]
      ],
    });
  }

  submitLoginForm() {
    this.login();
  }

  login() {
    const isValid = this.validateForm()
    if (isValid) {
      const credentials = this.sanitizeCredentials();
      this.authService.login(credentials)
        .subscribe(res => {
          if (res['status'] == 200) {
            this.router.navigate(['/home/tabs/tab1']);
          }
        }, error => {
          console.error(error);
        }
        );
    } else {
      this.presentToast();
      return;
    }
  }

  validateForm() {
    return this.loginForm.valid ? true : false;
  }

  sanitizeCredentials() {

    const [account, holder, password] = [
      this.loginForm.get('account').value,
      this.loginForm.get('holder').value,
      this.loginForm.get('password').value
    ];

    return {
      account,
      password,
      holder
    };
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Incorrect input fields. Try again',
      duration: 2000
    });
    toast.present();
  }
}
