import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StatementsService {

  uri = `${environment.URI_API}/user/statements?start_date=2020-03-01&end_date=2020-05-15`;

  constructor(private http: HttpClient) { }

  fetchUserStatements(): Observable<any> {
    console.log('entrei')
    return this.http.get(this.uri);
  }
}
